######## 	Magento2


#######		ENV FILE
MYSQL_HOST=db
MYSQL_ROOT_PASSWORD=myrootpassword
MYSQL_USER=magento
MYSQL_PASSWORD=magento
MYSQL_DATABASE=magento

MAGENTO_LANGUAGE=en_GB
MAGENTO_TIMEZONE=Pacific/Auckland
MAGENTO_DEFAULT_CURRENCY=NZD
MAGENTO_URL=http://local.magento

MAGENTO_ADMIN_BACKEND=admin123
MAGENTO_ADMIN_FIRSTNAME=Admin
MAGENTO_ADMIN_LASTNAME=MyStore
MAGENTO_ADMIN_EMAIL=amdin@example.com
MAGENTO_ADMIN_USERNAME=admin
MAGENTO_ADMIN_PASSWORD=magentorocks1

MAGENTO_RUN_MODE=developer
PHP_MEMORY_LIMIT=2048M
PHP_ENABLE_XDEBUG=false
UPDATE_UID_GID=true
DEBUG=false
ENABLE_SENDMAIL=true
UPLOAD_MAX_FILESIZE=64M
USE_SECURE_URL=0
USE_SECURE_ADMIN_URL=0
PMA_HOST=mysql


#######

/etc/hosts
172.18.0.2 db

#######




####### example --> container_name ---> Magento2_web_1

$ docker exec -it Magento2_web_1 install-magento


#######	 Magento 2

$ docker exec -it <container_name> install-magento

#######	 Sample data


$ docker exec -it <container_name> install-sampledata


#######	Deploy

$ docker exec -it <container_name> deploy

#######  OPTIONAL*

static file problem

insert core_config_data (config_id, scope, scope_id, path, value) values (null, 'default', 0, 'dev/static/sign', 0);


#######!#########

